const initialState = {
  selectedPackage: 0,
  packages: [],
  channels: [],
  packageChannels: [],
  filters: {
    period: ['cesoir'],
    category: [],
  },
  filteredPrograms: [],
  searchResults: [],
  program: {}
}

export default initialState