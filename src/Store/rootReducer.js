import packages from '../Services/Packages/reducer'
import channels from '../Services/Channels/reducer'
import selectedPackage from '../Services/SelectedPackage/reducer'
import packageChannels from '../Services/PackageChannels/reducer'
import filters from '../Services/Filters/reducer'
import filteredPrograms from '../Services/FilteredPrograms/reducer'
import searchResults from '../Services/SearchResults/reducer'
import program from '../Services/Program/reducer'

export default (state = {}, action) => {
  return {
    packages: packages(state.packages, action, state),
    selectedPackage: selectedPackage(state.selectedPackage, action, state),
    channels: channels(state.channels, action, state),
    packageChannels: packageChannels(state.packageChannels, action, state),
    filters: filters(state.filters, action, state),
    filteredPrograms: filteredPrograms(state.filteredPrograms, action, state),
    searchResults: searchResults(state.searchResults, action, state),
    program: program(state.program, action, state)
  };
};
