import React, { Component } from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import * as searchResultsActions from '../../Services/SearchResults/actions'

import axios from 'axios'

import Header from '../../Components/Header/Header'
import ProgramsList from '../../Components/ProgramsList/ProgramsList'
import SectionTitle from '../../Components/SectionTitle/SectionTitle'

import './Search.scss'

class Search extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoaded: false
    }
  }

  loadSearchResults() {
    axios.get('http://programmetv.digimondo.net/tv/search-program/' + this.props.match.params.pattern)
      .then(res => {
        this.props.searchResultsActions.loadSearchResults(res.data.result)
        this.setState({isLoaded : true})
      })
  }
  componentDidMount(){
    this.loadSearchResults()
  }
  
  componentDidUpdate(prevProps) {
    if (this.props.match.params.pattern !== prevProps.match.params.pattern) {
      this.loadSearchResults()
    }
  }
  
  render() {
    return (
      <div className="qvapp">
        <Header />
        {this.state.isLoaded &&
          <div className="qvapp-search">
            {this.props.searchResults.broadcasts.length > 0 &&
              <div>
                <SectionTitle title={"Diffusions : (" + this.props.searchResults.broadcasts.length + ")"} />
                <ProgramsList programs={this.props.searchResults.broadcasts} startIndex={0} count={200} />
              </div>
            }
            {this.props.searchResults.replay.length > 0 &&
              <div>
                <SectionTitle title={"Replays : (" + this.props.searchResults.replay.length + ")"} />
                <ProgramsList programs={this.props.searchResults.replay} startIndex={0} count={200} />
              </div>
            }
            {this.props.searchResults.netflix.length > 0 &&
              <div>
                <SectionTitle title={"Netflix : (" + this.props.searchResults.netflix.length + ")"} />
                <ProgramsList programs={this.props.searchResults.netflix} startIndex={0} count={200} />
              </div>
            }
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = ({searchResults}) => ({
  searchResults
})

const mapDispatchToProps = (dispatch) => ({
  searchResultsActions: bindActionCreators(searchResultsActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Search);