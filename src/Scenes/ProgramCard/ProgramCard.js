import React, { Component } from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import * as programActions from '../../Services/Program/actions'

import axios from 'axios'

import Header from '../../Components/Header/Header'
import ChannelLogo from '../../Components/ChannelLogo/ChannelLogo';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar, faPlay } from '@fortawesome/free-solid-svg-icons'

import './ProgramCard.scss'

class ProgramCard extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoaded: false,
      isVideoPlayed : false
    }

    this.handleBAClick = this.handleBAClick.bind(this);
  }

  componentDidMount(){
    axios.get('http://programmetv.digimondo.net/tv/get-program-by-id/' + this.props.match.params.id)
      .then(res => {
        this.props.programActions.loadProgram(res.data)
        this.setState({isLoaded : true})
      })
  }

  handleBAClick() {
    this.setState(prevState => ({
      isVideoPlayed: !prevState.isVideoPlayed
    }))

    const vid = document.querySelector('#qvapp-ba')
    this.state.isVideoPlayed ? vid.pause() : vid.play()

    vid.removeEventListener('ended', () => {})
    vid.addEventListener('ended', () => {
      this.setState({isVideoPlayed : false})
      if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen()
      } else {
        document.webkitCancelFullScreen()
      }
    })
  }
  
  render() {
    const prg = this.props.program
    return (
      <div className="qvapp">
        <Header />
        {this.state.isLoaded && typeof prg === 'object' &&
          <div className="qvapp-programCard">
            <div className="bg-image">
              <img src={prg.photo} alt="" />
            </div>
            <div className="qvapp-programCard-informations">
              <div className={(this.state.isVideoPlayed) ? "main-image video-played" : "main-image"}>
                {prg.photo && <img src={prg.photo} alt="" /> }
                {typeof prg.videos === 'object' && Object.keys(prg.videos).length > 0 && 
                  <video id="qvapp-ba" controls src={prg.videos[Object.keys(prg.videos)[0]].url}></video>
                }
              </div>
              <div className="main-infos">
                <h1>
                  {prg.title}
                  {prg.csa !== '' &&
                    <span className="pastille csa">{prg.csa}</span>
                  }
                </h1>
                {prg.stars !== "0" &&
                  <span className="rating">
                    {Array(+prg.stars).fill(null).map((str,i) => (
                      <FontAwesomeIcon key={i} icon={ faStar } size="xs" color="#0093d6" />
                    ))}
                  </span>
                }
                {prg.season_number && prg.episode_number && prg.subtitle !== '' &&
                  <span className="episode">Saison {prg.season_number} - Episode {prg.episode_number} : {prg.subtitle}</span>
                }
                {prg.subtitle !== '' &&
                  <span className="episode">{prg.subtitle}</span>
                }

                {/*
                <div className="program-tags">
                  <span className="inedit">Inédit</span>
                </div>
                */}

                <div className="category-year">
                  {prg.subtype && prg.subtype.indexOf(prg.type) < 0 &&
                    <span>{prg.type}</span>
                  }
                  <span> {prg.subtype && prg.subtype.indexOf(prg.type) < 0 &&
                      <span className="sep">|</span>
                    } {prg.subtype}
                  </span>
                  {prg.year !== "0" &&
                    <span> <span className="sep">|</span> {prg.year}</span>
                  }
                </div>
                
                {typeof prg.casting === 'object' && prg.casting.Présentateur && prg.casting.Présentateur.length &&
                  <div className="infos">
                    <span>Présenté par :</span> 
                    <ul>
                    {
                      prg.casting.Présentateur.map(function(real, i){
                        if (i <  3)
                          return <li key={i}>{real.firstname} {real.lastname}</li>
                        else
                          return null
                      })
                    }
                    </ul>
                  </div>
                }
                {typeof prg.casting === 'object' && prg.casting.Réalisateur && prg.casting.Réalisateur.length &&
                  <div className="infos">
                    <span>Réalisé par :</span> 
                    <ul>
                    {
                      prg.casting.Réalisateur.map(function(real, i){
                        if (i <  3)
                          return <li key={i}>{real.firstname} {real.lastname}</li>
                        else
                          return null
                      })
                    }
                    </ul>
                  </div>
                }
                {typeof prg.casting === 'object' && prg.casting.Acteur && prg.casting.Acteur.length &&
                  <div className="infos">
                    <span>Avec :</span> 
                    <ul>
                    {
                      prg.casting.Acteur.map(function(actor, i){
                        if (i <  3000)
                          return <li key={i}>{actor.firstname} {actor.lastname}</li>
                        else
                          return null
                      })
                    }
                    </ul>
                    {prg.casting.Acteur.length > 3000 &&
                      <a href="#block-casting" className="button">Voir tout le casting</a>
                    }
                  </div>
                }

                {prg.synopsis &&  prg.synopsis !== '' &&
                  <div className="infos">
                    <span>Synopsis :</span> {prg.synopsis}
                  </div>
                }

                {prg.broadcasts && prg.broadcasts.length > 0 &&
                  <div className="infos">
                    <span>Prochaine diffusion :</span> Le Lundi 12 Décembre 2017 sur <ChannelLogo channelId={prg.broadcasts[0].name_simple_channel} /> 
                    {prg.broadcasts.length > 1000 &&
                      <a href="#block-diffusions" className="button">Voir toutes les  diffusions</a>
                    }
                  </div>
                }

                <ul className="video-links">
                  {typeof prg.videos === 'object' && Object.keys(prg.videos).length > 0 &&
                    <li>
                      <span className="link-teaser" onClick={this.handleBAClick}><span className="wrap-icon"><FontAwesomeIcon icon={ faPlay } size="xs" /></span> Bande-annonce</span>
                    </li>
                  }
                  {prg.in_netflix === '1' &&
                    <li><a className="link-netflix" href="http://www.netflix.com/fr/">Regarder sur<img width="60" src="https://cdn.worldvectorlogo.com/logos/netflix-2.svg" alt="Netflix" /></a></li>
                  }
                </ul>
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = ({program}) => ({
  program
})

const mapDispatchToProps = (dispatch) => ({
  programActions: bindActionCreators(programActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(ProgramCard);