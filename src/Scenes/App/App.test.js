import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios'
import App from './App';
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';

import {Provider} from 'react-redux'
import configureStore from '../../Store/configureStore'

it('renders without crashing', () => {
  const div = document.createElement('div');
  const store = configureStore();
  
  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path='/' component={App} />
        </Switch>
      </Router>
    </Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
