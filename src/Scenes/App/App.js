import React, { Component } from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import * as packagesActions from '../../Services/Packages/actions'
import * as channelsActions from '../../Services/Channels/actions'
import * as packageChannelsActions from '../../Services/PackageChannels/actions'
import * as filteredProgramsActions from '../../Services/FilteredPrograms/actions'

import axios from 'axios'

import './App.scss'
import Header from '../../Components/Header/Header'
import FiltersList from '../../Components/FiltersList/FiltersList'
import InsideTopTeaser from '../../Components/InsideTopTeaser/InsideTopTeaser'
import ProgramsList from '../../Components/ProgramsList/ProgramsList'

class App extends Component {

  loadFilteredPrograms() {
    const params = new URLSearchParams(this.props.location.search)
    const date = params.get('date')
    const dateUrlParameter = (date) ? '/(date)/' + date : ''

    axios.get('http://programmetv.digimondo.net/tv/list-packages')
      .then((res) => {
        this.props.packagesActions.loadPackages(res.data.bouquets)
        axios.get('http://programmetv.digimondo.net/tv/grid-full' + dateUrlParameter)
          .then((res) => {
            this.props.channelsActions.loadChannels(res.data.channels)
            this.props.packageChannelsActions.loadPackageChannels(res.data.channels)
            this.props.filteredProgramsActions.loadFilteredPrograms()
          }
        )
      }
    )
  }
  
  componentDidMount(){
    this.loadFilteredPrograms()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.search !== prevProps.location.search) {
      this.loadFilteredPrograms()
    }
  }

  render() {
    return (
      <div className="qvapp">
        <Header />
        <FiltersList filters={this.props.filters} packages={this.props.packages} />
        {this.props.filteredPrograms.length > 0 &&
          <div className="qvapp-content">
            <ProgramsList programs={this.props.filteredPrograms} startIndex={0} count={200} />
            <InsideTopTeaser target="top_series_ete" color="#df26f5" title="Le top des séries à ne pas rater cet été" />
            <InsideTopTeaser target="top_videos_reseaux_sociaux" color="#80d326" title="Vidéos les + chocs sur les réseaux cette semaine" />
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = ({packages, channels, filters, filteredPrograms}) => ({
  packages, channels, filters, filteredPrograms
})

const mapDispatchToProps = (dispatch) => ({
  packagesActions: bindActionCreators(packagesActions, dispatch),
  channelsActions: bindActionCreators(channelsActions, dispatch),
  packageChannelsActions: bindActionCreators(packageChannelsActions, dispatch),
  filteredProgramsActions: bindActionCreators(filteredProgramsActions, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(App);