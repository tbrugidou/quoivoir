import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import './ProgramsList.scss'

import ProgramTeaser from '../ProgramTeaser/ProgramTeaser'

class ProgramsList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      programs : this.props.programs || [],
      startIndex : this.props.startIndex || 0,
      count : this.props.count || (this.programs.length - this.startIndex),
      limit : this.props.limit || 12
    }

    this._handleSeeMoreClick.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    this.setState({programs: nextProps.programs})
  }

  _handleSeeMoreClick = () => {
    this.setState({limit: this.state.limit + 12})
  }

  render() {
    const maxHeight = 'calc(100vw * (390/700) * ' + Math.floor(this.state.limit / 4) + ' / 4 + 80px)';
    return (
      <div>
        <div className="qvapp-programsList"  style={{maxHeight: maxHeight}}>
            {this.state.programs
              .filter( (prg, i) => (i >= this.state.startIndex && (i - this.state.startIndex) < this.state.count) && i < this.state.limit + 4 )
              .map( (prg, i) => (
                <Link to={"/programme/" + prg.id} key={i}>
                  <ProgramTeaser program={prg} />
                </Link>
              ))
            }
        </div>
        {Math.min(this.props.count,this.state.programs.length) > this.state.limit &&
          <div className="qvapp-seemoreButton">
            <span onClick={this._handleSeeMoreClick}>Voir +</span>
          </div>
        }
      </div>
    );
  }
}

export default ProgramsList