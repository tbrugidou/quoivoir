import React, { Component } from 'react'

import './FiltersList.scss'

import Filter from './Filter/Filter'
import PackagesList from './PackagesList/PackagesList'
import DatePicker from './DatePicker/DatePicker'

class FiltersList extends Component {

  isItActiveCategory(categoryId) {
    return ( this.props.filters.category.indexOf(categoryId) >=0 )
  }
  
  isItActivePeriod(period) {
    return ( this.props.filters.period.indexOf(period) >=0 )
  }

  render() {
    return (
      <div className="qvapp-filtersList">
        <div>
          <DatePicker refreshPrograms={this.props.refreshPrograms} value="" locale="fr" dateFormat="dddd DD MMMM" viewMode="days" timeFormat={false} />
        </div>
        <div>
          <Filter isRadio id="period" value="maintenant" name="Maintenant" active={this.isItActivePeriod('maintenant')} />
          <Filter isRadio id="period" value="cesoir" name="Ce Soir" active={this.isItActivePeriod('cesoir')} />
        </div>
        <div>
          <Filter id="category" value="cinema" name="Films" active={this.isItActiveCategory('cinema')} />
          <Filter id="category" value="series" name="Séries" active={this.isItActiveCategory('series')} />
          <Filter id="category" value="sport" name="Sport" active={this.isItActiveCategory('sport')} />
          <Filter id="category" value="divertissement" name="Divertissement" active={this.isItActiveCategory('divertissement')} />
          <Filter id="category" value="magazine" name="Magazine" active={this.isItActiveCategory('magazine')} />
          <Filter id="category" value="culture" name="Culture" active={this.isItActiveCategory('culture')} />
          <Filter id="category" value="docu" name="Documentaires" active={this.isItActiveCategory('docu')} />
        </div>
        <div>
          <PackagesList packages={this.props.packages} />
        </div>
      </div>
    );
  }
}

export default FiltersList