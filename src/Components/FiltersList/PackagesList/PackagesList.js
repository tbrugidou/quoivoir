import React, { Component } from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import * as selectedPackageActions from '../../../Services/SelectedPackage/actions'
import * as packageChannelsActions from '../../../Services/PackageChannels/actions'
import * as filteredProgramsActions from '../../../Services/FilteredPrograms/actions'

import './PackagesList.scss'

class PackagesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: this.props.active || false
    }
    this._onChange = this._onChange.bind(this)
  }

  _onChange() {
    this.props.selectedPackageActions.setSelectedPackage(+document.querySelector('.qvapp-packagesList').value)
    
    this.props.packageChannelsActions.loadPackageChannels()
    this.props.filteredProgramsActions.loadFilteredPrograms()
  }

  render() {
    return (
      <select onChange={this._onChange} defaultValue={this.props.selectedPackage} className="qvapp-packagesList">
        {this.props.packages.length > 0 &&
          this.props.packages.map((pck,i) => {
            return <option key={i} value={i}>{pck.name}</option>
          })
        }
      </select>
    );
  }
}

const mapStateToProps = ({selectedPackage}) => ({selectedPackage})

const mapDispatchToProps = (dispatch) => ({
  selectedPackageActions: bindActionCreators(selectedPackageActions, dispatch),
  packageChannelsActions: bindActionCreators(packageChannelsActions, dispatch),
  filteredProgramsActions: bindActionCreators(filteredProgramsActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(PackagesList);