import React, { Component } from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import * as filtersActions from '../../../Services/Filters/actions'
import * as filteredProgramsActions from '../../../Services/FilteredPrograms/actions'

import './Filter.scss'

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: this.props.active || false
    }
    this._handleClick = this._handleClick.bind(this)
  }

  _handleClick = () => {
    if (this.props.isRadio && this.state.isActive) return;

    this.props.filtersActions.setFilter({
      id: this.props.id,
      value: this.props.value,
      isRadio: this.props.isRadio
    })
    
    this.setState((prevState) => ({isActive: !prevState.isActive}))
    
    this.props.filteredProgramsActions.loadFilteredPrograms()
  }
  
  componentWillReceiveProps(nextProps) {
    this.setState({isActive: nextProps.active})
  }

  render() {
    return (
      <li onClick={this._handleClick} className={(this.state.isActive) ? "qvapp-filter active " + this.props.id : "qvapp-filter " + this.props.id}>
        {this.props.name}
      </li>
    )
  }
}

const mapStateToProps = () => ({})

const mapDispatchToProps = (dispatch) => ({
  filtersActions: bindActionCreators(filtersActions, dispatch),
  filteredProgramsActions: bindActionCreators(filteredProgramsActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Filter);