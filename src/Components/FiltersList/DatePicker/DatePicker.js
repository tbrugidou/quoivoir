import React, { Component } from 'react'

import { withRouter } from 'react-router-dom'

import './RDTCoreCss.scss'
import './DatePicker.scss'

import Datetime from 'react-datetime'

import moment from 'moment'
import 'moment/locale/fr'

class DatePicker extends Component {
  constructor(props) {
    super(props)

    let date = new Date()
    const params = new URLSearchParams(props.location.search)
    if (params.get('date')) {
      date = moment(params.get('date'), 'DD-MM-YYYY').toDate()
    }

    this.state = {
      date: date
    }

    this._handleDateChange = this._handleDateChange.bind(this)
  }

  _handleDateChange = (date) => {
    let nextRoute = '/'

    const today = new Date()
    if (date.date() !== today.getDate()) {
      let formatedDate = date.format('DD-MM-YYYY')
      nextRoute = '/?date=' + formatedDate
    }

    this.props.history.push(nextRoute)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.search !== this.props.location.search) {
      let date = new Date()
      const params = new URLSearchParams(this.props.location.search)
      if (params.get('date')) {
        date = moment(params.get('date'), 'DD-MM-YYYY').toDate()
      }
      this.setState({
        date: date
      })
    }
  }

  render() {
    console.log(this.state.date)
    const today = Datetime.moment().subtract('days', 1),
      maxDate = Datetime.moment().add('days', 15),

      valid = function( current ){
        return current.isAfter( today ) &&  current.isBefore( maxDate )
      }

    return (
      <Datetime closeOnSelect defaultValue={this.state.date} value={this.state.date} onChange={this._handleDateChange} isValidDate={valid} {...this.props} />
    );
  }
}

export default withRouter(DatePicker)