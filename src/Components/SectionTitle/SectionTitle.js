import React, { Component } from 'react'

import './SectionTitle.scss'

class SectionTitle extends Component {
  render() {
    return (
      <h2 className="qvapp-sectionTitle" style={{color: this.props.color}}>
          {this.props.title} 
      </h2>
    );
  }
}

export default SectionTitle