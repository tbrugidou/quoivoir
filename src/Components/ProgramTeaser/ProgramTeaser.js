import React, { Component } from 'react'

import ChannelLogo from '../ChannelLogo/ChannelLogo';

import './ProgramTeaser.scss'

class ProgramTeaser extends Component {
  render() {
    const prg = this.props.program
    const channelId = prg.channelId || (prg.channel && prg.channel.name_simple)

    return (
      <div className="qvapp-programTeaser">
          <span className="title">{prg.title}</span>
          <img alt="" src={prg.photo} />
          {channelId && <ChannelLogo channelId={prg.channelId || (prg.channel && prg.channel.name_simple)} /> }
          <span className="menu"></span>
          <span className="time">{prg.time}</span>
      </div>
    );
  }
}

export default ProgramTeaser