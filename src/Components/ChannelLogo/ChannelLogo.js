import React, { Component } from 'react'

import './ChannelLogo.scss'

class ChannelLogo extends Component {
  constructor(props) {
    super(props)

    this.color = props.color || 'rgb(128,128,128)'
  }

  render() {
    return (
      <span className={"qvapp-channelLogo bg-" + this.props.channelId}></span>
    );
  }
}

export default ChannelLogo