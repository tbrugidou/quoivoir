import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import logo from '../../Assets/Images/qvlogo.jpg'
import Button from './Button/Button'
import SearchForm from './SearchForm/SearchForm'

import './Header.scss'

class Header extends Component {
  constructor() {
    super()

    this.state = {
      searchIsActive: true
    }
    this._handleSearchClick = this._handleSearchClick.bind(this)
  }

  _handleSearchClick = () => {
    this.setState((prevState) => ({searchIsActive: !prevState.searchIsActive}))
    
    setTimeout(
      () => {
        document.querySelector('.qvapp-searchForm > input[type=text]').focus()
      }
    , 10)
  }

  renderSearchForm() {
    return (this.state.searchIsActive) ?  <SearchForm /> : null
  }
  renderSearchButton() {
    return <Button name="search" onClick={this._handleSearchClick} />
  }
  
  render() {
    return (
      <header className="qvapp-header">
        <Button name="burger" />
        <Link to="/">
          <img src={logo} className="qvapp-logo" alt="logo" />
        </Link>
        {this.renderSearchForm()}
        <Button name="favorites" />
      </header>
    );
  }
}

export default Header