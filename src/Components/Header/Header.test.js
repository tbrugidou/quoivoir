import Header from './Header';
import React from 'react';
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';


import renderer from 'react-test-renderer'

describe('Header component renders correctly', () => {
  it('renders correctly', () => {
    const rendered = renderer.create(
      <Router>
        <Header />
      </Router>
    );
    expect(rendered.toJSON()).toMatchSnapshot();
  });
});