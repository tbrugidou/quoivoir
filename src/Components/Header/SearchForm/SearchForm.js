import React, { Component } from 'react'

import { withRouter } from 'react-router-dom'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

import './SearchForm.scss'

class SearchForm extends Component {
  constructor() {
    super()
    
    this._handleSubmit = this._handleSubmit.bind(this)
  }

  _handleSubmit = (e) => {
    e.preventDefault()
    const pattern = document.querySelector('#qvapp-inputSearch').value
    if (pattern !== '') {
      this.props.history.push('/recherche/' + pattern)
    }
  }

  render() {
    return (
        <form onSubmit={this._handleSubmit} className="qvapp-searchForm">
          <input id="qvapp-inputSearch" type="text" placeholder="Rechercher un programme" />
          <button onClick={this._handleSubmit}>
            <FontAwesomeIcon icon={ faSearch } />
          </button>
        </form>
    );
  }
}

export default withRouter(SearchForm)