import React, { Component } from 'react'

import './Button.scss'


class Button extends Component {
  render() {
    return (
      <button className={'qvapp-btn qvapp-' + this.props.name} onClick={this.props.onClick}></button>
    );
  }
}

export default Button