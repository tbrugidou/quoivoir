import React, { Component } from 'react'

import logoBlanc from '../../Assets/Images/qvlogo_blanc.png';
import './InsideTopTeaser.scss'

class InsideTopTeaser extends Component {
  constructor(props) {
    super(props)

    this.state = {
      color : props.color || 'rgb(128,128,128)'
    }
  }
  render() {
    return (
      <a className="qvapp-insideTopTeaser">
      <div className="text-layer">
        <span className="title">{this.props.title}</span>
        <span className="selection-redac">sélection de la rédac <img alt="quoivoir" src={logoBlanc} /></span>
      </div>
      <div className="color-layer" style={{backgroundColor: this.state.color}}></div>
        <div className="wrapper-image">
          <img alt="" src={window.location.origin + '/img/' + this.props.target + '.jpg'} />
        </div>
      </a>
    );
  }
}

export default InsideTopTeaser