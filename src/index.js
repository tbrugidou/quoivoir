import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import App from './Scenes/App/App';
import ProgramCard from './Scenes/ProgramCard/ProgramCard';
import Search from './Scenes/Search/Search';

import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux'
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';

import configureStore from './Store/configureStore'

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path='/' component={App} />
        <Route path='/programme/:id?' component={ProgramCard} />
        <Route path='/recherche/:pattern?' component={Search} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();