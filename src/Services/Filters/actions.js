import * as types from './actionTypes'

export const setFilter = (payload) => {
  return {
     type: types.SET_FILTER,
     payload
  }
}