import * as types from './actionTypes'
import initialState from '../../Store/initialState'


let filters = (state = initialState.filters, action, root) => {

  switch(action.type){
    case types.SET_FILTER:
      let newState = state

      if (action.payload.isRadio) newState[action.payload.id].pop()

      if (newState[action.payload.id].indexOf(action.payload.value) < 0) {
        newState[action.payload.id].push(action.payload.value)
      } else {
        newState[action.payload.id].splice(newState[action.payload.id].indexOf(action.payload.value), 1);
      }
      return newState
    default:
      return state
    }
}

export default filters;