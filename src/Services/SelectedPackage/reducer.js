import * as types from './actionTypes'
import initialState from '../../Store/initialState'


let selectedPackage = (state = initialState.selectedPackage, action, root) => {
  switch(action.type){
    case types.SET_SELECTED_PACKAGE:
      let newState = action.payload;
      return newState 

    default:
      return state

    }
}

export default selectedPackage;