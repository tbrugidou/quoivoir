import * as types from './actionTypes'

export const setSelectedPackage = (payload) => {
  return {
     type: types.SET_SELECTED_PACKAGE,
     payload
  }
}