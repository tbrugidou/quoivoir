import * as types from './actionTypes'
import initialState from '../../Store/initialState'


let channels = (state = initialState.channels, action, root) => {

  var newState = []
  switch(action.type){
    case types.LOAD_PACKAGE_CHANNELS:
      root.packages[root.selectedPackage || 0].channels.forEach((package_ch, index) => {
        newState.push(root.channels.filter(ch => 
          ch.id === package_ch.name
        )[0])
      })
      return newState

    default:
      return state
    }
}

export default channels;