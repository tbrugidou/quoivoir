import * as types from './actionTypes'

export const loadPackageChannels = (payload) => {
  return {
     type: types.LOAD_PACKAGE_CHANNELS,
     payload
  }
}