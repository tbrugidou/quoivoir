import * as types from './actionTypes'

export const loadChannels = (payload) => {
  return {
     type: types.LOAD_CHANNELS,
     payload
  }
}