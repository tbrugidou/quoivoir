import * as types from './actionTypes'
import initialState from '../../Store/initialState'


let channels = (state = initialState.channels, action, root) => {

  var newState = []
  switch(action.type){
    case types.LOAD_CHANNELS:
      newState.push(...action.payload)
      return newState

    default:
      return state
    }
}

export default channels;