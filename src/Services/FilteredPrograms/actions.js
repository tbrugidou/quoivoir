import * as types from './actionTypes'

export const loadFilteredPrograms = (payload) => {
  return {
     type: types.LOAD_FILTERED_PROGRAMS,
     payload
  }
}