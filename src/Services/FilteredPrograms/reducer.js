import * as types from './actionTypes'
import initialState from '../../Store/initialState'

let filteredPrograms = (state = initialState.filteredPrograms, action, root) => {

  const categories = {
    'cinema' : ['Film' , 'Téléfilm' , 'Court métrage' , 'Making-of'],
    'series' : ['Série', 'Série/Feuilleton' , 'Feuilleton'],
    'sport' : ['Sport', 'Emission sportive', 'Magazine sportif', 'Documentaire sportif'],
    'divertissement' : ['XY', 'Variétés' , 'Jeu' , 'Divertissement' , 'Talk-show' , 'Téléréalité' , 'Programme court'],
    'magazine' : ['Magazine' , 'Emission religieuse' , 'Débat' , 'Cérémonie', 'Journal' , 'Information' , 'Météo'],
    'jeunesse' : ['Jeunesse' , 'Dessin animé', 'Série fantastique'],
    'culture' : ['Musique' , 'Concert' , 'Clips' , 'Opéra', 'Spectacle' , 'Théâtre' , 'Ballet' , 'Danse' , 'Gala' , 'Cérémonie'],
    'docu' : ['Documentaire' , 'Série documentaire' , 'Reportage']
  };

	const convertTime = (time) => {
		if (!time) return 0;
    
		// convertit un temps genre '22h15' en minutes
		const hoursNext =  time.split('h')[0],
			  minutesNext = time.split('h')[1];

		return (parseInt(hoursNext, 10) * 60+ parseInt(minutesNext, 10));
  }

  const matchFilters = (programType) => {
    if (root.filters.category.length) {
      let match = false
      root.filters.category.forEach( (cat) => {
        if (categories[cat].indexOf(programType) >= 0) match = true
      })
      return match
    } else {
      return true
    }
  }
  
  switch(action.type){
    case types.LOAD_FILTERED_PROGRAMS:
      let newState = []
      
      root.packageChannels.forEach( (ch) => {
        ch.programs.forEach( (prg, index) => {
          prg.channelId = ch.id
          prg.channelName = ch.name
          
          if (!matchFilters(prg.type)) return;

          if (root.filters.period.indexOf('cesoir') >= 0) {
            if (prg.id_period === '1' || (index > 1 && prg.id_period === '2')) {
              newState.push(prg)
            }
          } else if (root.filters.period.indexOf('maintenant') >= 0) {
            let d = new Date(),
              nowMinutes = d.getHours() * 60 + d.getMinutes(),
              beginTime = convertTime(prg.time),
              endTime = beginTime + parseInt(prg.duration_broadcast, 10)

            if (prg.id_period === '-1') endTime -= 24 * 60;

            if (
              (prg.id_period === '-1' && nowMinutes < endTime)
              || (nowMinutes >= beginTime && nowMinutes <  parseInt(prg.duration,10) + beginTime)
            ) {
              newState.push(prg)
            }
          } else {
            newState.push(prg)
          }


        })
      })
      
      return newState

    default:
      return state
    }
}


export default filteredPrograms;