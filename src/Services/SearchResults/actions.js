import * as types from './actionTypes'

export const loadSearchResults = (payload) => {
  return {
     type: types.LOAD_SEARCH_RESULTS,
     payload
  }
}