import * as types from './actionTypes'
import initialState from '../../Store/initialState'


let searchResults = (state = initialState.searchResults, action, root) => {

  var newState = []
  switch(action.type){
    case types.LOAD_SEARCH_RESULTS:
    newState = action.payload
    return newState

    default:
      return state
    }
}

export default searchResults;