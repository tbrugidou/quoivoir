import * as types from './actionTypes'
import initialState from '../../Store/initialState'


let packages = (state = initialState.packages, action, root) => {

  switch(action.type){
    case types.LOAD_PACKAGES:
      let newState = [];
      newState.push(...action.payload);
      return newState

    default:
      return state
    }
}

export default packages;