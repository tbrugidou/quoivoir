import * as types from './actionTypes'

export const loadPackages = (payload) => {
  return {
     type: types.LOAD_PACKAGES,
     payload
  }
}