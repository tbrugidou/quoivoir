import * as types from './actionTypes'
import initialState from '../../Store/initialState'


let program = (state = initialState.program, action, root) => {

  switch(action.type){
    case types.LOAD_PROGRAM:
      let newState = action.payload;
      return newState 

    default:
      return state
    }
}

export default program;