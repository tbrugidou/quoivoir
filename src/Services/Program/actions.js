import * as types from './actionTypes'

export const loadProgram = (payload) => {
  return {
     type: types.LOAD_PROGRAM,
     payload
  }
}